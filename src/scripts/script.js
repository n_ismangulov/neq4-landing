$(document).ready(function() {

	$('body').addClass('loaded');



	// Menu item click
	(function() {
		var $item = $('.js-menu-link'),
			$body = $("html, body");

		$item.click(function(){
			var $t = $(this),
				href = $t.attr("href"),
				$target = $(href),
				offset = $target.offset().top;

			$body.animate({
				scrollTop: offset
			}, 300);

			return true;
		})
	})();

	// Menu active state follows scroll
	(function() {
		var $item = $('.js-menu-link');

		// Is visible function
		var isVisible = function(id) {

			var $elem = $(id),
				offsetTop,
				scrollTop;

			if ($elem.length < 1) {
				return false;
			}

			offsetTop = $elem.offset().top;
			scrollTop = $(window).scrollTop();

			// Prevent script for lat element
			if (scrollTop >= $('body').height() - $(window).height() - 10) {
				if (id == '#docs') {
					return true;
				}
				else {
					return false;
				}
			}

			if (scrollTop > offsetTop - 250) {
				return true;
			}
			return false;
		};

		$(window).scroll(function() {
			$item.each(function() {
				var $a = $(this),
					href = $a.attr("href");

				if (isVisible(href)) {
					$item.removeClass("active");
					$a.addClass("active");
					return;
				}

			});
		});
	})();

	// Show menu on scroll
	(function() {
		var $window = $(window),
			$menu = $('#side-menu'),
			timer;

		$window.scroll(function() {
			$menu.addClass('visible');

			clearTimeout(timer);
			timer = setTimeout(function() {
				$menu.removeClass('visible');
			}, 1000);
		});
	})();

	// Show scroll-button on scroll
	(function() {
		var $window = $(window),
			$button = $('#scroll-button'),
			topLimit = 70,
			$requestSection = $('#request');

		$window.scroll(function() {
			var top = $window.scrollTop(),
				requestSectionTop = $requestSection.offset().top - 500,
				requestSectionBottom = requestSectionTop + $requestSection.outerHeight();

			if (top > topLimit && (top < requestSectionTop || top > requestSectionBottom)) {
				$button.addClass('scroll-button_visible');
			}
			else {
				$button.removeClass('scroll-button_visible');
			}
		});
	})();

	// Smooth scroll
	(function() {
		var $link = $('.js-smooth-scroll'),
			$body = $("html, body");

		$link.click(function(){
			var $t = $(this),
				href = $t.attr("href"),
				$target = $(href),
				offset = $target.offset().top;

			$body.animate({
				scrollTop: offset
			}, 600);

			return false;
		})
	})();

	// Select2
	(function() {
		if (typeof $.fn.select2 !== 'undefined') {
			$('.js-select').select2({
				minimumResultsForSearch: 9999,
				width: '100%'
			});
		}
	})();

	//= parts/example-tabs.js
});

$('form :input').change(function() {
  var rooms = $('#rooms').val();
	var area = $('#area').val();
	var price_per_pano_max = 3000;
	var price_per_pano_middle = 1800;
	var price_per_pano_min = 1500;
	var min_price = 15000;
	var panos_per_rooms = 0;
	var panos_per_area = 0;
	var panos_number = 0;
	var final_price = 0;

	if (rooms === '') {
		rooms = 0;
	}

	if (area === '') {
		area = 0;
	}

	if (rooms === 1) {
		panos_per_rooms = 2;
	} else {
		panos_per_rooms = Math.ceil(rooms * 1.5);
	}

	panos_per_area = Math.ceil(area / 50);
	panos_number = Math.max(panos_per_rooms, panos_per_area) + 2;

	if (panos_number < 11) {
		final_price = panos_number * price_per_pano_max;
	} else if (panos_number > 50) {
		final_price = (panos_number - 50) * price_per_pano_min + 10 * price_per_pano_max + 40 * price_per_pano_middle;
	} else if (panos_number > 10) {
		final_price = (panos_number - 10) * price_per_pano_middle + 10 * price_per_pano_max;
	}

	if (rooms > 0 || area > 0) {
		$('#price').text(Math.max(min_price, final_price).toLocaleString('ru-RU'));
		$('#price_input').val(Math.max(min_price, final_price).toLocaleString('ru-RU'));
	} else {
		$('#price').text('0');
		$('#price_input').val('0');
	}
});

(function ($) {
	var sentForm = function (form, options) {

		//DOM
//		var btn = form.find(':submit');
		var btn = form.find('.submit');
		var loader	=	form.find('.loader');
		var wrapper = form.find('.form_wrapper');
		var success_result = form.find('.form_success');
		var fail_result	= form.find('.form_fail');
		var notification = form.find('.notification');
		var color = options.color;

		//Arrays
		var fields = [];
		var requied = [];

		//Fill array with form fields
		form.find('.field').each(function () {
			fields.push($(this));
		})

		fields.reverse();


		//Create array of requied fields and fill with Cookies if needed
		$.each(fields, function (k, e) {
			name = e.attr('name');
			attr = e.attr('required');
			if (typeof attr !== 'undefined' && attr !== false)
				requied.push(e);
		});


		//Hide errors when value is gonna change
		form.find('.field').bind('keypress keyup keydown change click', function () {

			var field = $(this);
			var min = field.attr('minlength');

//			console.log('this: '+field+' min:'+min);
			if (field.val().length >= min) {
				HideFieldError(field);
			}

		});




		//Enable/Disable on keypress submit button
		form.find('.field').bind('keypress keyup keydown change click', function () {

			$.each(requied, function (k, e) {
				n = e.attr('name');
				m = e.attr('minlength');
				if (e.val().length < m) {
					btn.attr('disabled', 'disabled');
					console.log("DISABLE BUTTON");
					notification.show();
					return false;
				} else {
					console.log("ENABLE BUTTON");

					btn.removeAttr('disabled');
					notification.hide();
				}
			});
		});


		btn.click(function() {
			form.submit();
		})

		//submit form
		form.submit(function () {

			//disable button
			btn.attr('disabled', 'disabled');

			/*
			 loader.krutilka({
			 color: color,
			 petals: 8, // Сколько лепестков у крутилки
			 size:20
			 });
			 */
			loader.trigger('show');


			//get form values
			var data = form.serialize() + '&ajax=1';

			//define ajax
			var request = $.ajax({
				url: options.url,
				cache: false,
				type: 'POST',
				data: data,
				dataType: 'json'
			});

			//Show form when click a in result block
			success_result.find('.button').click(function () {

				//clear all fields
				$.each(fields, function (k, e) {
					e.val('');
				});

				//disable submit button
				btn.attr('disabled', 'disabled');

				//show notification
				notification.show();


				success_result.hide(0, function (){
					wrapper.fadeIn(500);
				})

			})




			//if request done succsesfully
			request.done(function (d) {

				//call callBack function
				options.callBack.call(this, d);

				//switch result
				switch (d.result) {

					//SUCCESS
					case 'success':
						//success code here
						console.log('RESULT_SUCCESS');
						wrapper.hide(0, function (){
							success_result.fadeIn(500);
						})


					case 'notvalid':
						//disable button
						btn.attr('disabled', 'disabled');


						//proccess erros with form fields
						$.each(d.errors, function (k,e) {
							console.log('key:'+k+' element:'+e);
							var field = form.find('*[name=' + e + ']');

							//Show error for all fields in errors array
							ShowFieldError(field);
						});
						break;



						break;

					//If result IS NOT success

					default:
						//fail code here

						console.log('FAIL(SERVER)');
						wrapper.hide(0, function (){
							fail_result.fadeIn(500);
						})
						break;
				}
			});

			//if ajax request fails call callback function with error info
			request.fail(function (jqXHR, textStatus, errorThrown) {
//				alert('jqXHR: '+jqXHR);
//				alert('textStatus: '+textStatus);
//				alert('errorThrown: '+errorThrown);

				console.log('AJAX_FAIL');
				wrapper.hide(0, function (){
					fail_result.fadeIn(500);
				})

				var e = {
					"result": "fail",
					"status": textStatus,
					"erorr": errorThrown
				};

				options.callBack.call(this, e);
				btn.removeAttr('disabled');
			});

			request.always(function (jqXHR, textStatus, errorThrown) {
				loader.trigger('hide');
			});


			return false;

		});
	};

	/*
	 var show_error = function (e, c, m) {
	 var t = e.attr('type');
	 var m = m || false;
	 if (t != 'submit' && t != 'button') {
	 e.focus();
	 }
	 e.closest('td').removeClass().addClass(c);
	 if (m) {
	 e.after('<strong>' + m + '</strong>');
	 }
	 return true;
	 }
	 */

	/*
	 var valid = function (e) {
	 td = e.closest('td');
	 td.removeClass();
	 td.find('strong').remove();
	 }
	 */
	var ShowFieldError = function (field) {
		var type = field.attr('type');
//		var m = m || false;
		if (type != 'submit' && type != 'button') {
			field.focus();
		}

		field.closest('div').find('.field_error').slideDown(100);
		/*
		 if (m) {
		 e.after('<strong>' + m + '</strong>');
		 }
		 */
		return true;
	}

	var HideFieldError = function (field) {
		field.closest('div').find('.field_error').slideUp(100);
		return true;
	}





	$.fn.sentForm = function (o) {
		var options = {
			/*
			 color: '#808080', //krutilka color
			 size: 20, // Ширина и высота блока с крутилкой
			 petals: 8, // Сколько лепестков у крутилки
			 petalWidth: 2, // Толщина лепестка
			 petalLength: 9, // Длина лепестка
			 petalOffset: 2, // Внутреннее поле блока с крутилкой (расстояние до кончика лепестка)
			 time: 500, // Время прохода круга в миллисекундах
			 color: '#ffffff', // Цвет лепестков, можно RGBA
			 background: 'none' // Фон крутилки
			 */
			url: this.attr('action'), //ajax url, dafault get url from "action" form attribute
			callBack: function (e) {
				//CallBack function
				//"e" is JSON format {"result":STRING,"errors":[ARRAY],"fieldClear":STRING}

				/*
				 Yandex metrika reach goal with params

				 var params = {
				 "result": e.result
				 };
				 yaCounterXXXXXXXX.reachGoal('form', params);
				 */

				console.log(e)
			}
		};

		$.extend(options, o);
		this.each(function () {
			var el = $(this);
			sentForm(el, options);
		});

		return this;
	};

})(jQuery);

$("form").sentForm({
	url: "form.php",
	callBack: function (e) {
		console.log(e);

		$('.cta-block__input-error').hide();
		$('.form__error').hide();

		if (e.result == 'success') {
			if (e.form == 'calc') {
				$('#success-calc').show();
        $(document).trigger("formSent", ['Form_zayavka', 'Zayavka_sent']);
      } else if (e.form == 'callback') {
				$('#success-callback').show();
        $(document).trigger("formSent", ['Form_callback', 'CallBack_sent']);
      }
			$('#' + e.form).trigger("reset");
			$('#price').text('0');
		} else if (e.result == 'notvalid') {
			$.each(e.errors, function (index, value) {
				if (e.form == 'calc') {
					$('#' + e.form).find("input[name='" + value + "']").next('.form__error').show();
				} else if (e.form == 'callback') {
					$('#' + e.form).find("input[name='" + value + "']").next('.cta-block__input-error').show();
				}
			})
		}
	}
});