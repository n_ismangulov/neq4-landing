// Example tabs
(function() {
	var $tabLinks = $('.js-example-tab-link'),
		$tabs = $('.js-example-tab');


	$tabLinks.click(function() {
		var $t = $(this),
			$tab = $($t.data('tab'));

		$tabs.hide();
		$tab.show();

		$tabLinks.removeClass('example-block__links-item_active');
		$t.addClass('example-block__links-item_active');

		return false;
	});
})();