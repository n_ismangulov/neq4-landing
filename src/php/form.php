<?php
error_reporting(-1);
header( "Cache-Control: no-cache, must-revalidate" );
header( "Content-Type: text/json; charset=utf-8" );
header( "Access-Control-Allow-Methods: POST" );

require(__DIR__ . '/../vendor/autoload.php');

class Client
{
    const BASE_URL = 'https://new4.amocrm.ru';
    private $guzzle;
    private $login;
    private $hash;

    public function __construct($login, $hash)
    {
        $this->guzzle = new \GuzzleHttp\Client(['cookies' => true]);
        $this->login = $login;
        $this->hash = $hash;
    }

    public function unsorted(array $params = [])
    {
        $url = $this->createUrl('/api/unsorted/add/');
        $url = $url . '?api_key=' . $this->hash . '&login=' . $this->login;

        return $this->makePostRequest($url, ['form_params' => $params]);
    }

    private function createUrl($urlPath)
    {
        return self::BASE_URL . $urlPath;
    }

    private function makePostRequest($url, $params = [])
    {
        return $this->makeRequest('POST', $url, $params);
    }

    private function makeRequest($type, $url, $params = [])
    {
        try {
            $response = $this
                ->guzzle
                ->request($type, $url, $params)
                ->getBody()
                ->getContents();

            return json_decode($response, true);
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
}

$data = [
    'form'  => isset($_POST['form']) ? $_POST['form'] : '',
    'name'  => isset($_POST['name']) ? $_POST['name'] : '',
    'email' => isset($_POST['email']) ? $_POST['email'] : '',
    'phone' => isset($_POST['phone']) ? $_POST['phone'] : '',
    'area'  => isset($_POST['area']) ? $_POST['area'] : '',
    'rooms' => isset($_POST['rooms']) ? $_POST['rooms'] : '',
    'price' => isset($_POST['price']) ? $_POST['price'] : '',
];

$data_errors = [];

$validation = array(
    'name'  => '/(.+){2,}/',
    'phone' => "/^((\+)?[1-9]{1,4})?([-\s\.\/])?((\(\d{2,4}\))|\d{3,4})(([-\s\.\/])?[0-9]{2,6}){2,6}(\s?(ext|x|внутр|доб|#)(\.)?\s?[0-9]{1,6})?$/"
);

if ($data['form'] === 'calc') {
    $validation['email'] = "/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD";
    if (!empty($data['area'])) {
        $validation['area'] = "/^[0-9]*$/";
    }
    if (!empty($data['rooms'])) {
        $validation['rooms'] = "/^[0-9]*$/";
    }
}

foreach ($validation as $field => $regex) {
    $input = $data[$field];
    if (empty($input) OR !preg_match($regex, $input)) {
        array_push($data_errors, $field);
    }
}

if (!empty($data_errors))
    die(json_encode([
        'result' => 'notvalid',
        'form' => $data['form'],
        'errors' => $data_errors, //array with novalid fields
        'crm' => 'not sent'
    ])
);

$leadName = '';

if ($data['form'] === 'calc') {
    $leadName = 'Заявка с лендинга ' . date('H:i d.m.Y');
    $from = 'Заявка ' . date('H:i d.m.Y');
} else {
    $leadName = 'Запрос звонка с лендинга ' . date('H:i d.m.Y');
    $from = 'Запрос звонка ' . date('H:i d.m.Y');
}

$client = new Client('masha@neq4.ru', '0ec03bd34f8093ec1db69b6ed16e141f');

/** @var \GuzzleHttp\Psr7\Response $response */
$response = $client->unsorted([
    'request' => [
        'unsorted' => [
            'category' => 'forms',
            'add' => [
                [
                    'source' => 'neq4.ru/yandexmaps',
                    'source_uid' => null,
                    'data' => [
                        'leads' => [
                            [
                                'name' => $leadName,
                                'price' => $data['price'],
                                'date_create' => time(),
                                'custom_fields' => [
                                    [
                                        // метраж
                                        'id' => 573346,
                                        'values' => ['value' => $data['area']],
                                    ],
                                    [
                                        // помещения
                                        'id' => 573350,
                                        'values' => ['value' => $data['rooms']],
                                    ]
                                ],
                            ]
                        ],
                        'contacts' => [
                            [
                                'name' => $data['name'],
                                'custom_fields' => [
                                    [
                                        'id' => 379726,
                                        'values' => [
                                            [
                                                'enum' => 898850,
                                                'value' => $data['phone'],
                                            ],
                                        ]
                                    ],
                                    [
                                        'id' => 379728,
                                        'values' => [
                                            [
                                                'enum' => 898862,
                                                'value' => $data['email'],
                                            ],
                                        ],
                                    ],

                                ],
                                'date_create' => time(),
                            ],
                        ],
                        'companies' => [],
                    ],
                    'source_data' => [
                        'data' => [
                            'name_1' => [
                                'type' => 'text',
                                'id' => 'name',
                                'element_type' => '1',
                                'name' => 'Имя',
                                'value' => $data['name'],
                            ],
                            '573346_1' => [
                                'type' => 'text',
                                'id' => '573346',
                                'element_type' => '2',
                                'name' => 'Площадь',
                                'value' => $data['area'],
                            ],
                            '573350_1' => [
                                'type' => 'text',
                                'id' => '573350',
                                'element_type' => '2',
                                'name' => 'Комнат',
                                'value' => $data['rooms'],
                            ],
                            'price_1' => [
                                'type' => 'text',
                                'id' => 'price',
                                'element_type' => '2',
                                'name' => 'Cтоимость',
                                'value' => $data['price'],
                            ],
                            '379726_1' => [
                                'type' => 'multitext',
                                'id' => '379726',
                                'element_type' => '1',
                                'name' => 'Телефон',
                                'value' => [$data['phone']],
                            ],
                            '379728_1' => [
                                'type' => 'multitext',
                                'id' => '379728',
                                'element_type' => '1',
                                'name' => 'E-mail',
                                'value' => [$data['email']],
                            ],
                        ],
                        'form_id' => 1,
                        'form_type' => 1,
                        'origin' => [
                            'ip' => $_SERVER['REMOTE_ADDR']
                        ],
                        'date' => time(),
                        'from' => $from,
                        'form_name' => 'Order',
                    ],
                ]
            ]
        ],
    ],
]);

$result = false;

if ($response['response']['unsorted']['add']['status'] === 'success') {
    $result = true;
}

die(json_encode([
    'result' => $result ? 'success' : 'failed',
    'form' => $data['form']
]));